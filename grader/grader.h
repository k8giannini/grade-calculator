#ifndef GRADER_H
#define GRADER_H

#include <QMainWindow>
#include <QVector>

namespace Ui {
class grader;
}

class grader : public QMainWindow
{
    Q_OBJECT

public:
    explicit grader(QWidget *parent = 0);
    ~grader();


signals:
    void addScore10B(QString);
    void addScore10C(QString);
    void MID2vsFP(QString);

private slots:

    void MID1(int);
    void MID2(int);
    void FIN(int);

    void hw1slot(int);
    void hw2slot(int);
    void hw3slot(int);
    void hw4slot(int);
    void hw5slot(int);
    void hw6slot(int);
    void hw7slot(int);
    void hw8slot(int);

    void setPercentagesA(bool);
    void setPercentagesB(bool);

    void setCourse(QString);




private:
    QString score;
    double numScore;
    int Midterm1;
    int Midterm2;
    int MAXMID;
    QVector<int> hwscores;
    int lowhw;
    double hwavg;
    int Final;
    QString course;

    double FWT;
    double MWT;
    double HWT;
    double FPWT;

    bool A;
    bool B;

    void calculateLowhw (){
        lowhw = 20;
        for(int i = 0; i < hwscores.size(); ++i){
            if(hwscores[i] < lowhw){
                lowhw = hwscores[i];
            }
        }
    }

    void calculateHWavg10B(){
        calculateLowhw();
        hwavg = 0;
        for(int i = 0; i < hwscores.size(); ++i){
            hwavg += hwscores[i];
        }
        hwavg -= lowhw;
        hwavg = 5*hwavg/7;
    }

    void calculateHWavg10C(){
        hwavg = 5*(hwscores[0] + hwscores[1] + hwscores[2])/3;
    }

    void emitAddScore(){

        if(course == "PIC 10C"){
            calculateHWavg10C();
            if(A){
                numScore = (FWT*Final) + (MWT*Midterm1) + (FPWT*Midterm2) + (HWT*hwavg);
                score = QString::number(numScore);
                emit addScore10C(score);
            }

            if(B){
                numScore = (FWT*Final) + (FPWT*Midterm2) + (HWT*hwavg);
                score = QString::number(numScore);
                emit addScore10C(score);
            }
        }


        if(course == "PIC 10B"){
            calculateHWavg10B();
            if(A){
                numScore = (FWT*Final) + (MWT*Midterm1) + (MWT*Midterm2) + (HWT*hwavg);
                score = QString::number(numScore);
                emit addScore10B(score);
            }

            if(B){
                numScore = (FWT*Final) + (MWT*MAXMID) + (HWT*hwavg);
                score = QString::number(numScore);
                emit addScore10B(score);
            }
        }
    }


    Ui::grader *ui;
};

#endif // GRADER_H
