#include "grader.h"
#include "ui_grader.h"

grader::grader(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::grader)
{
    ui->setupUi(this);

    hwscores = QVector<int>(8);
    for(int i = 0; i < hwscores.size();++i){
        hwscores[i] = 0;
    }

    course = "PIC 10B";

    Midterm1 = 0;
    Midterm2 = 0;
    Final = 0;
    MAXMID = 0;
    lowhw = 20;
    FPWT = 0.35;
}

grader::~grader()
{
    delete ui;
}
void grader::setCourse(QString s){
    course = s;
    if(course == "PIC 10C"){
        emit MID2vsFP("Final Project");
    }
    else{
        emit MID2vsFP("Midterm 2");
    }

    setPercentagesA(A);
}

void grader::FIN(int val){
    Final = val;
    emitAddScore();
}

void grader::MID1(int val){
    Midterm1 = val;
    if(Midterm1>Midterm2){
        MAXMID = Midterm1;
    }
    emitAddScore();
}

void grader::MID2(int val){
    Midterm2 = val;
    if(Midterm2>Midterm1){
        MAXMID = Midterm2;
    }
    emitAddScore();
}

void grader::setPercentagesA(bool clicked){
    if(course == "PIC 10B"){
        if(clicked){
            HWT = 0.25;
            MWT = 0.20;
            FWT = 0.35;
            A = true;
            B = false;
        }
        else{
            HWT = 0.25;
            MWT = 0.30;
            FWT = 0.44;
            B = true;
            A = false;
        }
    }

    if(course == "PIC 10C"){
        if(clicked){
            HWT = 0.15;
            MWT = 0.25;
            FWT = 0.30;
            A = true;
            B = false;
        }
        else{
            HWT = 0.15;
            MWT = 0;
            FWT = 0.50;
            B = true;
            A = false;
        }
    }

    emitAddScore();
}

void grader::setPercentagesB(bool clicked){

    if(course == "PIC 10B"){
        if(clicked){
            HWT = 0.25;
            MWT = 0.30;
            FWT = 0.44;
            B = true;
            A = false;
        }
        else{
            HWT = 0.25;
            MWT = 0.20;
            FWT = 0.35;
            A = true;
            B = false;
        }
    }

    if(course == "PIC 10C"){
        if(clicked){
            HWT = 0.15;
            MWT = 0;
            FWT = 0.50;
            B = true;
            A = false;
        }
        else{
            HWT = 0.15;
            MWT = 0.25;
            FWT = 0.30;
            A = true;
            B = false;
        }
    }

    emitAddScore();
}

void grader::hw1slot(int val){
    hwscores[0] = val;
    calculateLowhw();
    emitAddScore();
}
void grader::hw2slot(int val){
    hwscores[1] = val;
    calculateLowhw();
    emitAddScore();
}
void grader::hw3slot(int val){
    hwscores[2] = val;
    calculateLowhw();
    emitAddScore();
}
void grader::hw4slot(int val){
    hwscores[3] = val;
    calculateLowhw();
    emitAddScore();
}
void grader::hw5slot(int val){
    hwscores[4] = val;
    calculateLowhw();
    emitAddScore();
}
void grader::hw6slot(int val){
    hwscores[5] = val;
    calculateLowhw();
    emitAddScore();
}
void grader::hw7slot(int val){
    hwscores[6] = val;
    calculateLowhw();
    emitAddScore();
}
void grader::hw8slot(int val){
    hwscores[7] = val;
    calculateLowhw();
    emitAddScore();
}


